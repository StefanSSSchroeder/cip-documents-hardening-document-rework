# CIP Development process (SM-1)

## Table of contents

1. [Introduction](#introduction)
2. [Scope](#Scope)
3. [SM-1 : Development process](#developmentprocess)

***** 
<div style='page-break-after: always'></div>

## Revision History
| Revision No | Date       | Change description                                            | Author       | Reviewed by                               |
|-------------|------------|---------------------------------------------------------------|--------------|-------------------------------------------|
| 001         | 2022-12-20 | Draft document of CIP development process                        | Sai Ashrith | Dinesh Kumar |

****
<div style='page-break-after: always'></div>

## Introduction <a name="introduction"></a>

This document explains the development process used in CIP based on documented requirement checklist for SM-1 requirement in 62443-4-1. Design details such as system's devices and subsystems connections, exploitable areas and trust boundaries based on SD-1 requirement in 62443-4-1 are also included in this document.

## Scope <a name="Scope"></a>

This document explains the requirements which are clearly confirmed after discussion with Certification body. Some part of documentation is still not available due to need of further discussion with certification body such as configuration management document is SM-1. 

## SM-1 : Development process <a name="developmentprocess"></a>

The documentations and details required to fulfill SM-1 requirement in CIP are mentioned below :

  * Configuration Management document is available [here](./secure_development_process.md#21-configuration-management). It is incomplete as detail to be covered here will be added after discussion with certification body.

  * Audit logging procedures implemented in CIP and the measures taken to counter audit log failures are mentioned in this [document](../security/CR2.10_Response_to_audit_process_failure.md).
  
  * Requirements definition details of CIP are mentioned [here](https://www.cip-project.org/wp-content/uploads/sites/17/2018/10/CIP_Whitepaper_10.19.18.pdf) as project goals.

       Some of the project goals of CIP are :-

       1. Super Long Term Support (SLTS) for **CIP kernel** and **CIP Core** packages up to 10+ years.
       2. Regular testing of security updates on [CIP reference](https://wiki.linuxfoundation.org/civilinfrastructureplatform/ciptesting/cipreferencehardware) boards.
       3. Provide CIP reference images to CIP users.
       4. Each workgroup to have it's own focus areas.

       The CIP requirements is under review of CIP members, once finalised, respective details will be updated in this [document](./CIP_requirements.md).

  * **CIP-Core** re-uses **Debian** pre-built packages along with CIP-kernel which re-uses mainline kernel. The design and developments process which takes place in mainline kernel is mentioned [here](https://www.kernel.org/doc/html/latest/process/2.Process.html). CIP-Core design and development details are mentioned in this [CIP-Core Wiki page](https://wiki.linuxfoundation.org/civilinfrastructureplatform/cip-core) along with the [list of packages](https://gitlab.com/cip-project/cip-core/cip-pkglist) being used in the system. 

    Every package used from Debian in CIP-Core follows the below process before including in the design.

    ![Adding packages](../resources/images/other_documents/pdp_workflow.jpg).
  
  * **CIP-Core** implementation details are present in [CIP-Core Wiki page](https://wiki.linuxfoundation.org/civilinfrastructureplatform/cip-core) and the [isar-cip-core](https://gitlab.com/cip-project/cip-core/isar-cip-core/-/tree/master) repository is actively maintained by the CIP developers. CIP kernel is actively maintained by the CIP developers in [CIP Kernel](https://gitlab.com/cip-project/cip-kernel/linux-cip) repository.CIP kernel mostly re-uses mainline Linux kernel, so here is the [reference](https://linux-kernel-labs.github.io/refs/heads/master/lectures/intro.html) with Linux kernel implementation details.

    Here is an illustration which explains CIP-Core implementation :

    ![CIP-Core implementation](../resources/images/other_documents/cip-core-implementation.jpg)

  * **CIP-Core** relies on upstream testing as it re-uses Debian packages without any modifications. The rigorous testing process done by Debian is mentioned at [Debian Testing page](https://wiki.debian.org/DebianTesting).Debian also has it's own CI for testing Debian packages and the results of some packages are available [here](https://ci.debian.net/packages). CIP testing work group is responsible for **CIP Kernel testing** and those details are documented at [CIP Kernel testing page](https://wiki.linuxfoundation.org/civilinfrastructureplatform/ciptesting/ciptestingwg). Here is an illustration of testing overview in CIP. ![CIP Testing Overview](../resources/images/user_manual/cip-testing-overview.png).

  * The life-cycle details of **CIP-Core**, **CIP-Kernel** and their upstream projects are mentioned in [cip-lifecycle](https://gitlab.com/cip-project/cip-lifecycle) repository.
    Here is the CIP kernel life cycle support diagram ![CIP Kernel maintainence](../resources/images/other_documents/lifecycle.png).
