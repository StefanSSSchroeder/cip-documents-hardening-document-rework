
# <Center> Configuration Management </Center>

# Table of contents
1. [Objective](#Objective)
2. [Acronyms](#Acronyms)
3. [Content](#Content)
4. [CIP Kernel version](#CIP_Kernel_version)
5. [CIP Core version](#CIP_Core_version)



   ***** 
<div style='page-break-after: always'></div>

## Revision History
| Revision No | Date       | Change description          | Author       | Reviewed by                            |
|-------------|------------|-----------------------------|--------------|----------------------------------------|
| 001         | 2022-09-21 | Draft mostly empty document | Dinesh Kumar | To be reviewed with Certification Body |
| 002         | 2023-01-20 | Added version section       | Dinesh Kumar | To be reviewed with Certification Body |








<div style='page-break-after: always'></div>

***

## 1. Objective <a name="Objective"></a>

The primary objective of this document is to define CIP development configuration management as per IEC-62443-4-1 SM-1 requirement.

While creating this document CIP members do not have clear understanding what all parts of configuration management should be covered and would be applicable to CIP, as a result this document will be revised after discussion with Certification Body.


## 2. Acronyms <a name="Acronym"></a>

| Acronyms | Details                       |
|----------|-------------------------------|
| CIP      | Civil Infrastructure Platform |
| CB       | IEC certification Body        |

## 3. Content <a name="Content"></a>

As per discussion with Certification Body, Configuration management for CIP can include version of both 
CIP Kernel and CIP Core as these are the two key components.

The version information should be consistently updated in this document. In case if the version is maintained in some other document, here reference can be provided.

## 4. CIP Kernel version <a name="CIP_Kernel_version"></a>

This section to be updated as soon as CIP kernel team confirms about CIP Kernel version maintenance policy.

Following CIP wiki pages describe about CIP Kernel versions.

[CIP Kernel Maintenance](https://wiki.linuxfoundation.org/civilinfrastructureplatform/start#kernel_maintainership) page describes about CIP kernel versions maintained.

[CIP Kernel Maintenance Process](https://wiki.linuxfoundation.org/civilinfrastructureplatform/cipkernelmaintenance) page describes steps to be followed by CIP Kernel developers.


## 5. CIP Core version <a name="CIP_Core_version"></a>

CIP Core members are discussing how to define version for CIP Core meta-data. This section to be updated in future.