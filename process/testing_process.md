
# <Center>CIP Testing </Center>

# Table of contents
1. [Objective](#Objective)
2. [Scope](#Scope)
3. [Content](#Content)
3.1 [CIP Kernel Testing](#CIP_Kernel_Testing) 
3.2 [CIP Core Testing](#CIP_Core_Testing) 



   ***** 
<div style='page-break-after: always'></div>

## Revision History
| Revision No | Date       | Change description                 | Author   | Reviewed by                     |
|-------------|------------|------------------------------------|----------|---------------------------------|
| 001         | 2022-02-03 | Draft mostly empty document        | Yasin    | To be reviewed by Kento Yoshida |
| 002         | 2023-01-23 | Added CIP Kernel testing reference | Dinesh K | To be reviewed by SWG members.                            |








<div style='page-break-after: always'></div>

***

## 1. Objective <a name="Objective"></a>

The primary objective of this document is to list our current status regarding testing in CIP. It will be used as a basis to enhance our processes.


## 2. Scope <a name="Scope"></a>

There is no IEC 62443 requirement directly answered by this document.

## 3. Content <a name="Content"></a>

### 3.1  CIP Kernel Testing<a name="CIP_Kernel_Testing"></a>

CIP Testing workgroup does CIP Kernel testing to ensure before CIP Kernel release all tests are passed.
There are dedicated pages which describe about CIP Kernel testing.

CIP Centralized testing details can be found at https://wiki.linuxfoundation.org/civilinfrastructureplatform/ciptesting/centalisedtesting

CIP Testing is also described at https://wiki.linuxfoundation.org/civilinfrastructureplatform/ciptesting

CIP also has a CI running in gitlab which is used to test CIP kernel changes continuously at 

https://wiki.linuxfoundation.org/civilinfrastructureplatform/ciptesting/centalisedtesting/cioverview 

### 3.2  CIP Core Testing<a name="CIP_Core_Testing"></a>

CIP Core working group members create CIP reference images by using meta-data and recipes hosted at [isar-cip-core](https://gitlab.com/cip-project/cip-core/isar-cip-core/-/tree/master) repository.

isar-cip-core has CI setup for testing all changes for basic image creation.
Since isar-cip-core uses binary packages from Debian upstream repositories and all Debian packages are tested in their respective upstream repositories hence in isar-cip-core further testing of each package is not done.

However, isar-cip-core has an IEC layer which is tested by IEC tests hosted at [CIP Security tests](https://gitlab.com/cip-project/cip-testing/cip-security-tests) gitlab repository.





