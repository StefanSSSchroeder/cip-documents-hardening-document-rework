# CIP Security hardening

## Introduction

This document is intended to document CIP Security hardening guidelines and best practices. It will also help to meet IEC-62443-4-1 SG-3 process requirements.
Different CIP workgroup members investigated security hardening practices which will help to harden CIP based systems and make it hard to hack CIP based end products. As Security is always affected by several factors, it’s important for CIP users to keep in mind these guidelines and apply them based on specific use cases.

## Table of contents

1. [Scope](#Scope)
2. [Terms](#Terms)
3. [Hardening Guidelines](#hardeningguidelines)
4. [Technical Implementation details](#technicalimplementation)
5. [Normative references](#normativereferences)
6. [History](#History)

  ***** 
<div style='page-break-after: always'></div>

## Revision History
| Revision No | Date       | Change description                                            | Author       | Reviewed by                               |
|-------------|------------|---------------------------------------------------------------|--------------|-------------------------------------------|
| 001         | 2022-11-22 | Template of Security hardening in CIP                        | Dinesh Kumar ||
| 002         | 2022-11-22 | Details added regarding guidelines, technical implementation | Stefan Schroeder | |

****
<div style='page-break-after: always'></div>

## 1. Scope <a name="Scope"></a>

This manual makes no claim of completeness for any specific use-case, product or project. The applicability of every guideline provided here must be evaluated by the user. Adherence to all these rules does not guarantee a secure product, but serves as an additional puzzle-piece to reduce risk by reducing the attack surface. 

Nothing in this document should be taken to contradict standards and guidelines made mandatory by national legislation. It has been created for adopters of the CIP operating system.

## 2. Terms <a name="Terms"></a>

‘**(System) hardening**’: ‘**Hardening**’ or ‘**System hardening**’ is an umbrella term to denote a variety of activities to improve the overall security of an information system by reducing the attack surface.

Several methods are available to this end, depending on the phase of the development lifecycle:

During the **design** phase : A system architect can choose patterns, designs, or features based on their security merit. 

 * Example 1: A system may be designed to use the Telnet-protocol for remote access. A security expert would recommend to harden the system by replacing Telnet with a more secure protocol.
 
 * Example 2: A system architect may be installing an operating system using a convenient default install template, thus including hundreds of applications that are not required for the intended use. A security expert would recommend to build the system from the ground from a minimal base, adding only what is necessary to perform the task the system is designed for.

During the **implementation** phase: Developers can implement software using secure coding best-practices and guidelines.

 * Example: A developer may be offering to enter a password via the keyboard, echoing the letters as they are typed. A security expert would recommend to harden the implementation by replacing the letters with place-holders to prevent shoulder-surfing or accidental sharing during a video call. 
 
 During **builds**, **test-runs** and **deployment**: Testers can assess the integrity of build-artifacts by either Cryptographic integrity protection or provisioning of integrity information via separate channels to prevent tampering.
 
 * Example: A tester may receive a test-object via mail or file share with no option to verify its integrity. A security expert would recommend to sign artifacts digitally, encrypt them, or to provide file hashes via a second secure channel.
 
 During **commissioning** and **maintenance**: Maintainers can improve the security posture of a product or device using the available configuration options to reduce the attack surface.

 * Example: Maintainers may leave a deployed system in their original state as they were deployed. A security expert would recommend to review the configuration for potential security improvements, including but not limited to password updates, patching, and removal of unneeded functions.
 
 Above all detailed hardening tips, there exists a list of established security principles that users **violate** at their peril:
 
* **The need-to-know principle:** Information should only be made available to users that require the information.
* **The principle-of-least-privilege:** Access to functionality should only be made available to users that need it to fulfill the system’s intended purpose. 
* **Avoid any single-point-of-failure:** Any single device may fail. Do not allow the failure of individual objects to tear down your entire system. Be resilient. 
* **Separation-of-privilege:** Users of a system shall not have administrative privileges. 
* **Defence-in-depth:** Failure of a single security feature should NOT allow an attacker to easily move laterally and escalate their attack. 

**‘Hardening tools’:** Typically a system in a default state is not as secure as desired. There exists a variety of tools to harden a system based on predefined plans, templates and recommendations, e.g. [Lynis](https://cisofy.com/lynis/) or [openSCAP](https://www.open-scap.org/). Some of the tools provide not only a hardening-function, but also auditing/compliance check features.

## 3. Hardening Guidelines <a name="hardeningguidelines"></a>

This section provides a list of well-established hardening guidelines.

* Deactivate/disable/remove default users and passwords. If a default password must be used, provide a function to change the password at the earliest opportunity.

* Every human user should have an individual account. There should be no account-sharing. User accounts shall be disabled during off-boarding and it shall be cyclically checked if access is still required (e.g. by expiration). Separate administration accounts and user accounts.

* Remove all software packages and services that are not required for the use case, especially build-tools and any network programs. 

* Keep track of all the installed software packages cyclically and compare them to the desired state. Keep track of all the open network ports and cyclically compare them to the desired state. 

* Configure the firewall to the most restrictive permissible settings.

* Remove any hardware ports and devices that are not required for the use case.

* Keep the CIP based system up-to-date with respect to security-patches. Ensure that you install the latest security patches in a timely manner. CIP developers regularly share latest CVE information and CIP users are advised to use CVE information and update the system regularly to receive the latest CVEs fixes.

* Use recommended encryption algorithms/cipher suites (e.g. by [NIST SP 800-52](https://csrc.nist.gov/News/2019/nist-publishes-sp-800-52-revision-2)) and provide the option to upgrade encryption ciphers in the final product.

* Do not invent your own cryptography.

* If you are using antivirus-software, keep the virus-database up-to-date.

* Setup disaster recovery plans.

* Document and maintain secure configuration options through the entire life cycle of a product.

**Note:** As the CIP based system is based on the Debian operating system, you are advised to follow the [latest guidelines published by the Debian project](https://www.debian.org/doc/manuals/securing-debian-manual).

## 4. Technical Implementation details <a name="technicalimplementation"></a>

* To check the list of installed packages, run 

```
sudo dpkg-query -l | tee list_of_installed_packages.txt
```

* Integrity protection of the filesystem in Debian is provided by either [aide](https://packages.debian.org/bullseye/aide), [tripwire](https://packages.debian.org/bullseye/tripwire) or [samhain](https://packages.debian.org/bullseye/samhain). 

* Root-kit detection packages include chkrootkit, rkhunter and [debcheckroot](https://www.elstel.org/debcheckroot/) (not in included in Debian-repository 11-2022). 

* To investigate open network ports on a system use [nmap](https://packages.debian.org/stable/nmap).

## 5. Normative references <a name="normativereferences"></a>

There exists no agreed international ‘Hardening Standard’. A variety of organisations and institutions publish hardening guidelines to improve the security stance of information systems. The following list may serve as an introduction to the topic:

*   [NIST Special Publication 800-123: Guide to General Server Security](https://nvlpubs.nist.gov/nistpubs/legacy/sp/nistspecialpublication800-123.pdf)
*   [The Charter of Trust](https://www.charteroftrust.com/about/), specifically Principle 3
*   [CIS Operating System Hardening Benchmarks (members only)](https://www.cisecurity.org/benchmark/)
*   [STIGS by the US-DOD](https://public.cyber.mil/stigs/)
*   [SANS Security Policy Templates](https://www.sans.org/information-security-policy/)
*   Germany: [BSI ICS-Security Kompendium v 1.23](https://www.bsi.bund.de/SharedDocs/Downloads/DE/BSI/ICS/ICS-Security_kompendium_pdf.pdf?__blob=publicationFile)

All major operating systems provide hardening guidelines, e.g. [Red Hat](https://access.redhat.com/documentation/en-us/red_hat_enterprise_linux/8/html/security_hardening/index), [Suse](https://documentation.suse.com/sles/15-SP3/html/SLES-all/book-security.html), [Arch](https://wiki.archlinux.org/title/security), [Debian](https://www.debian.org/doc/manuals/securing-debian-manual/index.en.html) and [Microsoft](https://learn.microsoft.com/en-us/windows/security/).
The ISO/IEC 62443 4-1 does not require a specific set of hardening rules to be applied, but mandates the existence of a Hardening Guide for a product.

## 6. History <a name="History"></a>

Initial: Nov, 2022 by Dinesh Kumar and Stefan Schroeder.


