# <Center>CIP Security Hardening Standard </Center>

# Table of contents
1. [Objective](#objective)
2. [Assumptions](#assumptions)
3. [Scope](#scopes)
4. [Security Hardening Standard](#security_hardening_standard)
5. [Best Practices](#best_practices)
6. [Additional](#additional)


   *****
<div style='page-break-after: always'></div>

## Revision History
| Revision No | Date       | Change description         | Author       | Reviewed by                              |
|-------------|------------|----------------------------|--------------|------------------------------------------|
| 001         | 2022-03-04 | Draft CIP Security Hardening Standard | Djuned Fernando Djusdek | Reviewed: [1](https://gitlab.com/cip-project/cip-security/iec_62443-4-x/-/issues/37#note_864677868), [2](https://gitlab.com/cip-project/cip-security/iec_62443-4-x/-/issues/37#note_922498694) |
| 002         | 2022-05-12 | Simplified Best Practices to refer Upstream | Djuned Fernando Djusdek | To be reviewed by CIP Security WG members |
|             |            |                            |              |                                          |





****
<div style='page-break-after: always'></div>

















<div style='page-break-after: always'></div>

***

## 1. Objective <a name="objective"></a>

CIP security hardening standard provide instructions, rational and recommendations of CIP core and CIP kernel hardening for embedded and network devices.

> This document may become an object for revision in the future.

## 2. Assumptions <a name="assumptions"></a>

| Assumption                               | Impact |
|------------------------------------------|--------|
| Understanding of CIP core and CIP kernel | High   |

> See [CIP User Manual](https://gitlab.com/cip-project/cip-documents/-/blob/master/user/user_manual/user_manual.md).

## 3. Scope <a name="scopes"></a>

Scope of this document is the hardening standard of CIP core and CIP kernel, focus on embedded and network devices.

Embedded and network devices scope is as in IEC62443-4-2 standard:

1. Embedded device requirements (EDR); and,
2. Network device requirements (NDR).

In this document, there are two levels of implementation:

1. ***SHALL***: At this level CIP users can ignore and understand the risks; and,
2. ***MUST***: At this level CIP users must follow CIP instructions, rationale, and recommendations.

## 4. Security Hardening Standard <a name="security_hardening_standard"></a>

### 4.1. CIP Kernel Hardening

**Instructions**

CIP users SHALL apply CIP kernel updates on timely manner.

**Rationale**

1. Security fixes;
2. Stability improvements;
3. Updated drivers;
4. New kernel functions; and,
5. Increase speed.

> See [5 Reasons Why You Should Update Your Kernel Often [Linux]](https://www.makeuseof.com/tag/5-reasons-update-kernel-linux/) article for further explanation.

**Recommendations**

Subscribe to the cip-dev mailing list to get the latest information.

### 4.2. CIP Core Hardening

CIP prepare pre-installed security packages (and dependencies) for CIP core as, but is not limited to (*alphabetical order*):

| #   | Package                 | Debian Buster           | Debian Bullseye       |
| :-: | ----------------------- | :---------------------: | :-------------------: |
|  1. | `acl`                   | [see package](https://packages.debian.org/buster/acl)                   | [see package](https://packages.debian.org/bullseye/acl)                   |
|  2. | `aide`                  | [see package](https://packages.debian.org/buster/aide)                  | [see package](https://packages.debian.org/bullseye/aide)                  |
|  3. | `audispd-plugins`       | [see package](https://packages.debian.org/buster/audispd-plugins)       | [see package](https://packages.debian.org/bullseye/audispd-plugins)       |
|  4. | `auditd`                | [see package](https://packages.debian.org/buster/auditd)                | [see package](https://packages.debian.org/bullseye/auditd)                |
|  5. | `chrony`                | [see package](https://packages.debian.org/buster/chrony)                | [see package](https://packages.debian.org/bullseye/chrony)                |
|  6. | `fail2ban`              | [see package](https://packages.debian.org/buster/fail2ban)              | [see package](https://packages.debian.org/bullseye/fail2ban)              |
|  7. | `libtss2-esys-3.0.2-0`  | -                                                                       | [see package](https://packages.debian.org/bullseye/libtss2-esys-3.0.2-0)  |
|     | `libtss2-esys0`         | [see package](https://packages.debian.org/buster/libtss2-esys0)         | -                                                                         |
|  8. | `libpam-cracklib`       | [see package](https://packages.debian.org/buster/libpam-cracklib)       | [see package](https://packages.debian.org/bullseye/libpam-cracklib)       |
|  9. | `libpam-pkcs11`         | [see package](https://packages.debian.org/buster/libpam-pkcs11)         | [see package](https://packages.debian.org/bullseye/libpam-pkcs11)         |
| 10. | `nftables`              | [see package](https://packages.debian.org/buster/nftables)              | [see package](https://packages.debian.org/bullseye/nftables)              |
| 11. | `openssh-client`        | [see package](https://packages.debian.org/buster/openssh-client)        | [see package](https://packages.debian.org/bullseye/openssh-client)        |
| 12. | `openssh-server`        | [see package](https://packages.debian.org/buster/openssh-server)        | [see package](https://packages.debian.org/bullseye/openssh-server)        |
| 13. | `openssh-sftp-server`   | [see package](https://packages.debian.org/buster/openssh-sftp-server)   | [see package](https://packages.debian.org/bullseye/openssh-sftp-server)   |
| 14. | `openssl`               | [see package](https://packages.debian.org/buster/openssl)               | [see package](https://packages.debian.org/bullseye/openssl)               |
| 15. | `sudo`                  | [see package](https://packages.debian.org/buster/sudo)                  | [see package](https://packages.debian.org/bullseye/sudo)                  |
| 16. | `syslog-ng-core`        | [see package](https://packages.debian.org/buster/syslog-ng-core)        | [see package](https://packages.debian.org/bullseye/syslog-ng-core)        |
| 17. | `syslog-ng-mod-journal` | [see package](https://packages.debian.org/buster/syslog-ng-mod-journal) | [see package](https://packages.debian.org/bullseye/syslog-ng-mod-journal) |
| 18. | `tpm2-abrmd`            | [see package](https://packages.debian.org/buster/tpm2-abrmd)            | [see package](https://packages.debian.org/bullseye/tpm2-abrmd)            |
| 19. | `tpm2-tools`            | [see package](https://packages.debian.org/buster/tpm2-tools)            | [see package](https://packages.debian.org/bullseye/tpm2-tools)            |
| 20. | `uuid-runtime`          | [see package](https://packages.debian.org/buster/uuid-runtime)          | [see package](https://packages.debian.org/bullseye/uuid-runtime)          |

> Pre-installed Security Packages are defined in [ISAR CIP Core Images Security](https://gitlab.com/cip-project/cip-core/isar-cip-core/-/blob/master/recipes-core/images/cip-core-image-security.bb).

Further, see [Best Practices](#best_practices) section.

**Instructions**

CIP users MUST apply ISAR CIP core images security, which contains pre-installed security packages.

Further, CIP users SHALL update on timely manner.

**Rationale**

Due to lack of security packages, ISAR CIP core images cannot prevent any security vulnerabilities, exploits, and threats.

Meanwhile new security vulnerabilities, exploits, and threats can be exist or found in monthly, weekly or even on daily basis.

This lead CIP to prepare and maintain ISAR CIP core images security.

> See [Vulnerabilities, Exploits, and Threats: Defining three key terms in cybersecurity](https://www.rapid7.com/fundamentals/vulnerabilities-exploits-threats/) article for further understanding.

**Recommendations**

Apply ISAR CIP core images security rather than standard ISAR CIP core images.

### 4.3. Embedded and Network Devices Common Hardening

**Instructions**

CIP users MUST apply basic security such as, but not limited to:

1. Remove universal or standard password;
2. Update password on timely manner;
3. Use key management tools;
4. Use SSH key-pair authentication;
5. Limit access of applications, system and network;
6. Prevent user-space from root level access;
7. Use data-at-rest encryption;
8. Performing backup on timely manner; and,
9. Use trusted execution environment (TEE).

**Rationale**

***Universal Threat***

Universal or standard password is main vulnerability of embedded and network devices. Meanwhile, provider of embedded and network devices could not prepare product without this. From this point-of-view, CIP users MUST perform this by them-self.

Further, updating password is become requirement and CIP users MUST maintain by them-self, and securely store credential access on key management tools to prevent leak.

***Hardening Authentication and Limiting Access***

Updating authentication level to use SSH key-pair rather plain access is good choice to prevent leak. CIP users SHALL use SSH key-pair authentication to access their-own embedded and/or network devices, and prevent user-space to gain root level access.

Further, CIP users SHALL limiting access of their-own installed applications, system and network based-on their need. Such as, removing old protocol or closing unused port.

***Insecure Data Storage***

Non-encrypted data storages are become vulnerability for attacker. This can lead to data leak, lost or even stolen. To prevent this, CIP users MUST enable data-at-rest encryption.

Further, performing backup is also a good idea. Meanwhile, CIP users SHALL maintain their-own backup on safe place and uses encrypted storage with RAID support.

> See [M2: Insecure Data Storage](https://owasp.org/www-project-mobile-top-10/2016-risks/m2-insecure-data-storage) from [OWASP](https://owasp.org/) article for further understanding.

***Un-Trusted Environment Threat***

Due to possibility of data leak, executing applications on non-encapsulated (un-trusted environment) area is not recommended. Further, this could be exploited by attacker to attack the system level.

To prevent this, CIP users SHALL use trusted execution environment (TEE) to execute their-own or third party applications.

> See [What is a trusted execution environment (TEE) and how can it improve the safety of your data?](https://piwik.pro/blog/what-is-a-trusted-execution-environment/) article for further understanding.

**Recommendations**

Updating password on every half-year or shorter period is recommended.

Utilize RAID configuration for data storage to prevent local data loss.

Performing monthly or weekly basis backup is recommended.

> See [RAID](https://www.techtarget.com/searchstorage/definition/RAID) article for further understanding.

### 4.3. Embedded Devices Specific Hardening

**Instructions**

CIP users MUST apply basic security such as, but not limited to:

1. Update board support package (BSP).

**Rationale**

*TBD*

**Recommendations**

Further, see [Securing and Hardening Embedded Linux Devices: Theory and Practice](https://www.researchgate.net/publication/356361261_Securing_and_Hardening_Embedded_Linux_Devices_Theory_and_Practice).

### 4.4. Network Devices Specific Hardening

**Instructions**

CIP users MUST apply basic security such as, but not limited to:

1. Update firmware;
2. Backup configuration; and,
3. Employ firewall.

**Rationale**

*TBD*

**Recommendations**

Further, see [Hardening Network Devices](https://media.defense.gov/2020/Aug/18/2002479461/-1/-1/0/HARDENING_NETWORK_DEVICES.PDF) recommendations from [NSA](https://www.nsa.gov/).

## 5. Best Practices <a name="best_practices"></a>

This section provides best practices for hardening the system using pre-installed CIP core security packages.

**Instructions**

See [Securing Debian Manual](https://www.debian.org/doc/manuals/securing-debian-manual/index.en.html) and [Hardening](https://wiki.debian.org/Hardening).

**Rationale**

The security packages defined in the ISAR CIP core images security use pre-build packages provided from Debian Buster and/or Bullseye.

**Recommendations**

Further, CIP users are also advised to see/refer to the community and/or third-party applications guidelines.

## 6. Additional <a name="additional"></a>

There are several bodies that provide guidelines, but are not limited, such as by the [CIS](https://www.cisecurity.org/cis-benchmarks/) and [NIST](https://www.nist.gov/).

Further, CIP users is recommended to see best practices outlined by CIS in [BASICS OF THE CIS HARDENING GUIDELINES](https://blog.rsisecurity.com/basics-of-the-cis-hardening-guidelines/), and by NIST in [Special Publication (SP) 800-123](https://nvlpubs.nist.gov/nistpubs/Legacy/SP/nistspecialpublication800-123.pdf).
